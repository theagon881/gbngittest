﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;

public class AdsManager : Singleton<AdsManager>, IInterstitialAdListener, IBannerAdListener, INonSkippableVideoAdListener, IRewardedVideoAdListener
{

#if UNITY_EDITOR && !UNITY_ANDROID && !UNITY_IPHONE
        string appKey = "";
#elif UNITY_ANDROID
    // Puzzles & Solutions Android key
        string appKey = "976c638ed7c8e6b7c530d05105478dc0335fd80f825e65d8";
#elif UNITY_IPHONE
	//Tap2Play iOS key 
	//string appKey = "e602bb12aba4071e7fa36137f1fba389f7c578d63dfe0dc2";
    //Puzzles & Solutions key
    string appKey = "04bb69a54f173db9ebbec5ef68f65a496d0de370137f4ca4";
#else
        string appKey = "";
#endif

	private enum RewardedVideoState
    {
        Wait,
        Started,
        Finished
    }

    private struct RewardedVideoParams
    {
        public int amount;
        public string name;
    }

    public delegate void OnRewardedVideoDelegate(string name, int amount);

    public event OnRewardedVideoDelegate OnRewardedVideoFinished;

    private RewardedVideoState rewardedVideoState = RewardedVideoState.Wait;
    private RewardedVideoParams rewardedVideoParams = new RewardedVideoParams();

    // Use this for initialization
    void Start()
    {
#if UNITY_EDITOR
        Appodeal.setTesting(true);
#endif
        DontDestroyOnLoad(this.gameObject);

        Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, true);

        if( IsAdsRemoved )
        {
            Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, false);
            Appodeal.setAutoCache(Appodeal.BANNER, false);
			Appodeal.setAutoCache(Appodeal.INTERSTITIAL, false);
		}

        Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER | Appodeal.REWARDED_VIDEO);

        Appodeal.setBannerCallbacks(this);
        Appodeal.setInterstitialCallbacks(this);
        Appodeal.setRewardedVideoCallbacks(this);
        //Appodeal.setAutoCache();




    }


	public static void RemoveAds()
	{
		PlayerPrefs.SetInt("noads", 1);

		Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, false);
		Appodeal.setAutoCache(Appodeal.BANNER, false);
		Appodeal.setAutoCache(Appodeal.INTERSTITIAL, false);
        HideBanner();
	}

	public static bool IsAdsRemoved
	{
		get
		{
			return PlayerPrefs.GetInt("noads", 0) > 0 ? true : false;
		}
	}

    public static bool IsRewardedVideoLoaded
    {
        get
        {
#if UNITY_EDITOR
            return true;
#else
            return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
#endif
        }
    }

    public static void LoadRewardedVideo()
    {
        if (!IsRewardedVideoLoaded)
        {
            Appodeal.cache(Appodeal.REWARDED_VIDEO);
        }
    }

    public static bool IsInterstitialLoaded
    {
        get
        {
            return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
        }
    }

    public static void ShowInterstitial()
    {
        Appodeal.show(Appodeal.INTERSTITIAL, "interstitial_button_click");
    }


    public void ShowRewardedVideo()
    {
#if UNITY_EDITOR
        if (OnRewardedVideoFinished != null)
        {
            OnRewardedVideoFinished("coins", 99);
        }
#else
        Debug.Log("ShowRewardedVideo started");
        Appodeal.show(Appodeal.REWARDED_VIDEO);
        this.rewardedVideoState = RewardedVideoState.Started;
        StartCoroutine(WaitForVideoFinish());
#endif
    }

    public static void ShowBanner()
    {
        if (AdsManager.IsAdsRemoved == false)
        {
            Appodeal.show(Appodeal.BANNER_BOTTOM, "banner_button_click");
        }
    }

    public static void ShowInterstitialOrVideo()
    {
        if (AdsManager.IsAdsRemoved == false)
        {
            Appodeal.show(Appodeal.INTERSTITIAL | Appodeal.NON_SKIPPABLE_VIDEO);
        }
    }

    public static void HideBanner()
    {
        Appodeal.hide(Appodeal.BANNER);
    }

    private IEnumerator WaitForVideoFinish()
    {
        yield return new WaitUntil(() => this.rewardedVideoState == RewardedVideoState.Finished);
        this.rewardedVideoState = RewardedVideoState.Wait;
        if (OnRewardedVideoFinished != null)
        {
            OnRewardedVideoFinished(this.rewardedVideoParams.name, this.rewardedVideoParams.amount);
        }
        else
        {
            Debug.LogFormat("OnRewardedVideoFinished fucking null");
        }
    }

    public void onBannerClicked()
    {

    }

    public void onBannerFailedToLoad()
    {

    }

    public void onBannerLoaded()
    {

    }

    public void onBannerShown()
    {

    }

    public void onInterstitialClicked()
    {

    }

    public void onInterstitialClosed()
    {

    }

    public void onInterstitialFailedToLoad()
    {

    }

    public void onInterstitialLoaded()
    {

    }

    public void onInterstitialShown()
    {

    }

    public void onNonSkippableVideoClosed()
    {

    }

    public void onNonSkippableVideoFailedToLoad()
    {

    }

    public void onNonSkippableVideoFinished()
    {

    }

    public void onNonSkippableVideoLoaded()
    {

    }

    public void onNonSkippableVideoShown()
    {

    }

    public void onRewardedVideoClosed()
    {
        Debug.Log("onRewardedVideoClosed");
        this.rewardedVideoState = RewardedVideoState.Finished;
    }

    public void onRewardedVideoFailedToLoad()
    {
        Debug.Log("onRewardedVideoFailedToLoad");
        this.rewardedVideoState = RewardedVideoState.Finished;
    }

    public void onRewardedVideoFinished(int amount, string name)
    {
        Debug.LogFormat("onRewardedVideoFinished: {0} {1}", name, amount);
        this.rewardedVideoParams.amount = amount;
        this.rewardedVideoParams.name = name;
        this.rewardedVideoState = RewardedVideoState.Finished;
    }


    public void onRewardedVideoLoaded()
    {

    }

    public void onRewardedVideoShown()
    {
        Debug.Log("onRewardedVideoShown");
        this.rewardedVideoState = RewardedVideoState.Finished;
    }

}
