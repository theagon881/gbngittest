﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatisticValues
{
    Jump,
    TakeStar,
    GameCount,
}

public interface IAchievementEarnedListener
{
    void AchievementsEarned(List<string> achEarned);
}

public class AchievementData
{
    public AchievementInfo Info;
    public bool Earned;
    public int Value;
    public float Percent;
}

public class AchievementsController : Singleton<AchievementsController>
{
    private readonly Dictionary<StatisticValues, string> statNames = new Dictionary<StatisticValues, string>()
    {
        {StatisticValues.Jump, "StatJump"},
        {StatisticValues.TakeStar, "StatTakeStar"},
        {StatisticValues.GameCount, "StatGameCount"},
    };
    private Dictionary<StatisticValues, int> statValues = new Dictionary<StatisticValues, int>();
    private List<string> earnedAchievements = new List<string>();
    private AchievementList achievementsList;
    private IAchievementEarnedListener earnListener = null;

    void Start () {
        this.achievementsList = Resources.Load<AchievementList>("AchievementListData");
        this.statValues.Clear();
        this.earnedAchievements.Clear();

	    foreach (var curStatName in this.statNames)
	    {
	        int curValue = PlayerPrefs.GetInt(curStatName.Value);
            this.statValues[curStatName.Key] = curValue;
	        foreach (var curAchievement in this.achievementsList.Achievements)
	        {
	            if (curAchievement.StatValue == curStatName.Key
	                && curAchievement.Limit <= curValue
	                && !this.earnedAchievements.Contains(curAchievement.Id))
	            {
	                this.earnedAchievements.Add(curAchievement.Id);
	            }
	        }
	    }

        Debug.Log("Achievements controller started!");
        DontDestroyOnLoad(this.gameObject);
	}

    public void SetAchievementListener(IAchievementEarnedListener listener)
    {
        this.earnListener = listener;
    }

    public void Increment(StatisticValues statType, int increment)
    {
        int statValue = 0;
        if (this.statValues.TryGetValue(statType, out statValue))
        {
            statValue += increment;
        }
        else
        {
            statValue = increment;
        }
        this.statValues[statType] = statValue;
        PlayerPrefs.SetInt(this.statNames[statType], statValue);

        List<string> newAchievements = new List<string>();
        foreach (var curAchievement in this.achievementsList.Achievements)
        {
            if (curAchievement.StatValue == statType
                && curAchievement.Limit <= statValue
                && !this.earnedAchievements.Contains(curAchievement.Id))
            {
                this.earnedAchievements.Add(curAchievement.Id);
                newAchievements.Add(curAchievement.Id);
            }
        }

        if (newAchievements.Count > 0 && this.earnListener != null)
        {
            this.earnListener.AchievementsEarned(newAchievements);
        }
    }

    public List<AchievementData> GetAchievementsTable()
    {
        List<AchievementData> result = new List<AchievementData>();
        foreach (var curAchievement in this.achievementsList.Achievements)
        {
            AchievementData newData = new AchievementData();
            newData.Info = curAchievement;
            newData.Value = this.statValues[curAchievement.StatValue];
            newData.Earned = this.earnedAchievements.Contains(curAchievement.Id);
            newData.Percent = Mathf.Min((float)newData.Value / curAchievement.Limit, 1.0f);
            result.Add(newData);
        }

        return result;
    }
}
