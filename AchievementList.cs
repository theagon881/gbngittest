﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[Serializable]
public class AchievementInfo
{
    public string Id;
    public StatisticValues StatValue;
    public int Limit;
    public string DefaultTitle;
}

[CreateAssetMenu(fileName = "AchievementListData", menuName = "Rollex/AchievementList", order = 2)]
public class AchievementList : ScriptableObject
{
    public List<AchievementInfo> Achievements;
}